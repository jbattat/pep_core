C           DATA FROM PLANET,EARTH-MOON BARYCENTER,MOON TAPES
      COMMON/TAPDTA/ EMINT5,FEM(3),FM(3),FP(3),FREM1,FREM2,
     1 JDP(3),JDEM(3),JDM(3),JDP1,JDP2,JDEM1,JDEM2,JDM1,JDM2,
     2 JPLNT,INTPX,INTEMX,INTP5,
     3 IPAREM,IPARM,IPARP,LPAREM,LPARM,LPARP,IEVEL(3),IMVEL(3),IPVEL(3)
      REAL*10 EMINT5,FEM,FM,FP,FREM1,FREM2
      INTEGER*4 JDP,JDEM,JDM,JDP1,JDP2,JDEM1,JDEM2,JDM1,JDM2,JPLNT,
     1 INTPX,INTEMX,INTP5,IPAREM,IPARM,IPARP,
     2 LPAREM,LPARM,LPARP,IEVEL,IMVEL,IPVEL
